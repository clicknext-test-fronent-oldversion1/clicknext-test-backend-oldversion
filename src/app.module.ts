import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
// import { UsersModule } from './users/users.module';
// import { UsersController } from './users/users.controller';
// import { UserService } from './users/users.service';
import { User, UserSchema } from './users/user.model';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost:27017/cn-test'),
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
